package pertemuan5;

import java.util.Scanner;

public class Soal10 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan sebuah kata : ");
        String kata = s.next();
//        StringBuilder kataBalik = new StringBuilder();
//        kataBalik.append(kata);
//        kataBalik.reverse();
//        System.out.println(kataBalik);
//        if (kata.equals(kataBalik.toString())) {
//            System.out.println("Kata ini adalah Palindrome");
//        }else{
//            System.out.println("Bukan Palindrome");
//        }
        String kataBalik = "";
        for (int i = kata.length() - 1; i >= 0; i--) {
            kataBalik += kata.charAt(i);
        }
        if (kata.equals(kataBalik)) {
            System.out.println("Kata ini adalah Palindrome");
        } else {
            System.out.println("Bukan Palindrome");
        }

    }
}
