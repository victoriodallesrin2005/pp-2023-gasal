package pertemuan3;

public class TypeDataBooleanChar {

    public static void main(String[] args) {
        char a = 'A';
        char b = 65;
        System.out.println("Isi Char a = " + a);
        System.out.println("Isi Char b = " + b);

        //Tipe data boolean
        int c = 10, d = 14, e = 2, f = 12;
        boolean x = (c <= d) && (e < f) && (e > c);
        System.out.println("Hasil komparasi = " + x);
    }
}
