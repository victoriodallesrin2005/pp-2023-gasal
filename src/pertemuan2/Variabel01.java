package pertemuan2;

public class Variabel01 {

    public static void main(String[] args) {
        //deklarasi variabel
        int umur;
        //assign nilai ke variabel
        umur = Integer.MAX_VALUE;
        System.out.println("Isi variabel umur = " + umur);
        //tampilkan isi variabel
        umur = umur + 1;
        System.out.println("Isi variabel umur = " + umur);

    }
}
